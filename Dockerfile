#getting base image ubuntu
FROM ubuntu:18.04

RUN apt-get update



FROM nginx:alpine

MAINTAINER jjanevieve@gmail.com

SHELL ["bin/sh","-c"]

COPY index.php /tmp

ADD codefile /usr

RUN yum install -y which
RUN yum install -y net-tools

#Environment variable for setting up java home
ENV JAVA_HOME=/usr

EXPOSE 80 80

CMD /bin/bash
