FROM nginx:alpine

MAINTAINER jjanevieve@gmail.com

COPY . /usr/share/nginx/html

EXPOSE 80 80

CMD ["nginx","g","daemon off;"]

